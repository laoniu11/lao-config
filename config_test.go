package config_test

import (
	"log"
	"testing"

	config "gitee.com/laoniu11/lao-config"
)

func TestGetInt(t *testing.T) {
	v := config.GetInt("test.int.value", 11)
	if v == 11 {
		t.Errorf("failed to get int \n")
		t.Fail()
		return
	}

	t.Logf("get int value: %v\n", v)
}

func TestGetString(t *testing.T) {
	v := config.GetString("test3.xxx", "")
	if v != "xxxxasdasdasd" {
		t.Fail()
	}
}

func TestGetArray(t *testing.T) {
	v := config.GetArray[int]("test2")
	log.Println(v)
	if len(v) == 0 {
		t.Fail()
	}
	if v[0] != 2 && v[1] != 3 {
		t.Fail()
	}
}
