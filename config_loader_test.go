package config

import "testing"

func TestFill(t *testing.T) {
	target := map[any]any {
		"a": 1,
		"b": map[any]any {
			"b1": "b1_value",
		},
		"c": map[string]any {
			"c1": 3,
		},
		4: 3,
	}

	source := map[any]any {
		"a": "a1",
		"b": map[any]any {
			"b1": "b1_value_from_src",
			"b2": "b2_value_from_src",
		},
		"c": map[string]any {
			"c1": 4,
		},
		"d": map[string]any {
			"d1": 6,
		},
		4: 4,
		5: "5v",
	}

	fill(target, source)

	t.Logf("%+v", target)

	if target["a"] == 1 {
		t.Errorf("failed to check a value \n")
		t.FailNow()
	}

	newB1 := (target["b"].(map[any]any))["b1"].(string)
	if newB1 != "b1_value_from_src" {
		t.Errorf("b.b1: %v", newB1)
		t.Errorf("failed to check b.b1 value \n")
		t.FailNow()
	}

	if (target["b"].(map[any]any))["b2"] != "b2_value_from_src" {
		t.Errorf("failed to check b.b2 value \n")
		t.FailNow()
	}

	if target[4] != 4 {
		t.Errorf("failed to check 4 value \n")
		t.FailNow()
	}

	if target[5] != "5v" {
		t.Errorf("failed to check 5 value \n")
		t.FailNow()
	}

	if target["c"].(map[string]any)["c1"] != 4 {
		t.FailNow()
	}

	if target["d"].(map[string]any)["d1"] != 6 {
		t.FailNow()
	}
}

